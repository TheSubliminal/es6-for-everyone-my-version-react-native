import { createStackNavigator, createAppContainer } from 'react-navigation';
import FighterEdit from '../../components/FighterEdit/FighterEdit';
import Fighters from '../../components/Fighters/Fighters';

const FighterNavigator = createStackNavigator(
    {
        Fighters: {
            screen: Fighters,
            navigationOptions: {
                header: null
            }
        },
        FighterEdit: FighterEdit
    },
    {
        initialRouteName: 'Fighters'
    }
);

export default createAppContainer(FighterNavigator);