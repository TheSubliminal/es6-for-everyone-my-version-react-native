import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

const FighterItem = ({ id, name, source, onPress }) => {
    return(
        <TouchableOpacity onPress={() => onPress(id)}>
            <View style={styles.fighterContainer}>
                <Image source={{ uri: source }} style={{ width: 400, height: 400 }} resizeMode="contain" />
                <Text style={styles.name}>{name}</Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    fighterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    name: {
        fontSize: 20,
        fontFamily: 'Roboto',
        textAlign: 'center'
    }
});

export default FighterItem;