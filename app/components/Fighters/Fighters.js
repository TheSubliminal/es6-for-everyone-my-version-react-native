import React from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';
import FighterItem from '../FighterItem/FighterItem';
import { fighterService } from '../../services/fightersService';

class Fighters extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            fighters: []
        };

        this.onFighterEdit = this.onFighterEdit.bind(this);
    }

    componentDidMount() {
        fighterService.getFighters().then(fighters => {
            this.setState({
                loading: false,
                fighters
            });
        });
    }

    onFighterEdit(id) {
        this.props.navigation.push('FighterEdit', { id });
    }

    render() {
        const { fighters, loading } = this.state;

        return (
            <View style={styles.container}>
                {loading
                    ? <Text style={styles.message}>Loading...</Text>
                    : <FlatList
                        data={fighters}
                        renderItem={({ item }) => (<FighterItem id={item._id} name={item.name} source={item.source} onPress={this.onFighterEdit}/>)}
                        keyExtractor={item => item._id}
                    />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    message: {
        fontSize: 30
    }
});

export default Fighters;