import React, { useState } from 'react';
import { StyleSheet, View, Image, Text, TextInput, Button } from 'react-native';
import { fighterService } from '../../services/fightersService' ;

class FighterEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            error: null
        };

        this.onSave = this.onSave.bind(this);
        this.onStatChange = this.onStatChange.bind(this);
    }

    componentDidMount() {
        fighterService
            .getFighterDetails(this.props.navigation.getParam('id'))
            .then(({ _id, name, health, attack, defense, source }) => {
                this.initialStats = {
                    health: Number(health),
                    attack: Number(attack),
                    defense: Number(defense)
                };

                this.setState({
                    loading: false,
                    _id,
                    name,
                    health: Number(health),
                    attack: Number(attack),
                    defense: Number(defense),
                    source
                });
            })
            .catch(error => this.setState({ error }));
    }

    onSave() {
        const { _id, name, health, attack, defense, source } = this.state;
        if (!health || isNaN(health) ||
            !attack || isNaN(attack) ||
            !defense || isNaN(defense) ||
            ((health === this.initialStats.health) && (attack === this.initialStats.attack) && (defense === this.initialStats.defense))) {
            return;
        }

        this.setState({ submitting: true });
        fighterService
            .modifyFighter(this.props.navigation.getParam('id'), { _id, name, health, attack, defense, source })
            .then(() => {
                this.setState({ submitting: false })
            })
            .catch(error => this.setState({ error }));
    }

    onStatChange(statName, value) {
        this.setState({ [statName]: value });
    }

    render() {
        const { loading, error, name, health, attack, defense, source, submitting } = this.state;

        let content;
        if (error) {
            content = <Text style={styles.message}>{ error.message }</Text>
        } else if (loading) {
            content = <Text style={styles.message}>Loading...</Text>;
        } else {
            content = (
                <>
                    <Image source={{ uri: source }} style={{ width: 300, height: 300 }} resizeMode="contain" />
                    <Text style={styles.label}>{name}</Text>
                    <View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.label}>Health</Text>
                            <TextInput style={styles.inputField} placeholder='Health' value={health.toString()} onChangeText={(value) => this.onStatChange('health', +value)}/>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.label}>Attack</Text>
                            <TextInput style={styles.inputField} placeholder='Attack' value={attack.toString()} onChangeText={(value) => this.onStatChange('attack', +value)}/>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.label}>Defense</Text>
                            <TextInput style={styles.inputField} placeholder='Defense' value={defense.toString()} onChangeText={(value) => this.onStatChange('defense', +value)}/>
                        </View>
                    </View>
                    <Button style={styles.button} title="Save" onPress={this.onSave} disabled={submitting}/>
                </>
            );
        }

        return(
            <View style={styles.container}>
                {content}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    message: {
        fontSize: 30
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontSize: 20
    },
    inputField: {
        marginLeft: 10,
        marginBottom: 10,
        padding: 3,
        borderRadius: 5,
        fontSize: 20,
        backgroundColor: 'lightgrey'
    },
    button: {
        fontSize: 20
    }
});

export default FighterEdit;