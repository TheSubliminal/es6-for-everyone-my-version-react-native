import React from 'react';
import { FighterNavigator } from '../../routes/index';

const App = () => {
  return (
      <FighterNavigator />
  );
};

export default App;
